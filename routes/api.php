<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->prefix('users')->group( function () {

    Route::get('/profile', 'API\\UserController@getProfile');

    Route::get('/{id}', 'API\\UserController@getUser');

    Route::delete('/{id}', 'API\\UserController@deleteUser');

    Route::get('/', 'API\\UserController@allUsers');

    Route::put('/{id}', 'API\\UserController@updateUser');

    Route::put('/password', 'API\\UserController@changePassword');

});
