@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Profile') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('profile') }}">
                            @csrf
                            <label for="name"> {{ __('Name') }}</label>
                            <div class="form-group input-group">
                                <input id="name" type="text" name="name" required autocomplete="name"
                                       class="form-control @error('name') is-invalid @enderror"
                                       value="{!! $user->name !!}"/>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <label for="email"> {{ __('Email') }}</label>
                            <div class="form-group input-group">
                                <input id="email" type="email" name="email" required autocomplete="email"
                                       class="form-control @error('email') is-invalid @enderror"
                                       value="{!! $user->email !!}"/>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <label for="role">{{ __('Role') }}</label>
                            <div class="form-group input-group">
                                @if($user->role == "admin")
                                    <select id="role" name="role">
                                        <option value="admin" @if($user->role == "admin") selected @endif>admin</option>
                                        <option value="user">user</option>
                                    </select>
                                @else
                                    <input type="text" id="role" name="role" readonly value="{!! $user->role !!}"
                                           class="form-control">
                                @endif
                            </div>
                            <div class="form-group button-box button-box--multiple">
                                <a href="{{url('/changePassword')}}">Change password</a>
                                <input class="btn btn-primary" type="submit" value="{{ __('Save') }}"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

