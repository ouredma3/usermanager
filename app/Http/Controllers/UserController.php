<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Services\Attendance;
use  App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public $attendanceService;
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function getProfile()
    {
        return view('user.profile')->with('user' , Auth::user());
    }


    /**
     * Update authenticated user data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request)
    {

        try {
            $this->validate($request, [
                'name' => 'required|string' ,
                'email' => 'required|email',
                'role' => 'required'
            ], ['required' => 'The :attribute field is required.']);

            $user = Auth::user();
            if( $user->name != $request->name) {
                $user->name = $request->name;
            }
            if( $user->email != $request->email){
                $this->validate($request, [
                    'email' => 'unique:users']);
                $user->email = $request->email;
            }

            if( isset($request->role)) {
                if($user->role == "admin"){
                    $this->validate($request, [
                        'role' => 'in:admin,user']);
                }else{
                    $this->validate($request, [
                        'role' => 'in:user' ], ['Insufficient permissions']);
                }

                $user->role = $request->role;
            }
            $user->save();

            return back();

        } catch (\Exception $e) {
            return back()->with('error',$e->getMessage())->withInput();
        }

    }



    public function changePassword(Request $request){

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

    }


    /**
     * Update User data by id
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'role' => 'required|string',
            'email' => 'required|email|',
        ]);
        try {
            $user = User::findOrFail($id);
            if( $user->name != $request->name) {
                $user->name = $request->name;
            }

            if( $user->email != $request->email){
                $this->validate($request, [
                    'email' => 'unique:users']);
                $user->email = $request->email;
            }

            if( $user->role != $request->role) {
                
                $user->role = $request->role;
            }
            $user->save();
            return back();

        } catch (\Exception $e) {
            return back()->with('error',$e->getMessage())->withInput();
        }

    }

    /**
     * Delete user by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();

            return back();

        } catch (\Exception $e) {

            return back()->with('error',$e->getMessage())->withInput();
        }
    }

    public function logout()
    {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/');
    }

}