<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function allUsers()
    {
        return response()->json( User::all(), 200);
    }


    public function getProfile()
    {
        return response()->json(Auth::user(), 200);
    }

    public function getUser($id)
    {
        try {
            $user = User::findOrFail($id);

            return response()->json($user, 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|',
            'password' => 'confirmed',
        ]);

        try {
            $user = Auth::user();
            if( $user->name != $request->name) {
                $user->name = $request->name;
            }
            if( $user->email != $request->email){
                $this->validate($request, [
                    'email' => 'unique:users']);
                $user->email = $request->email;
            }
            if( isset($request->password)) {
                $plainPassword = $request->input('password');
                $user->password = app('hash')->make($plainPassword);
            }
            $user->save();

            return response()->json( $user, 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    public function updateUser(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'role' => 'required|string',
            'email' => 'required|email|',
            'password' => 'confirmed',
        ]);
        try {
            $user = User::findOrFail($id);
            if( $user->name != $request->name) {
                $user->name = $request->name;
            }
            if( $user->email != $request->email){
                $this->validate($request, [
                    'email' => 'unique:users']);
                $user->email = $request->email;
            }
            if( isset($request->password)) {
                $plainPassword = $request->input('password');
                $user->password = app('hash')->make($plainPassword);
            }
            $user->save();

            return response()->json($user, 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    public function deleteUser($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();

            return response()->json(['message' => 'user successfully deleted'], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }
    }

    public function changePassword(Request $request){

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return response()->json(['message' => 'Your current password does not matches with the password you provided. Please try again.'], 400);
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return response()->json(['message' => 'New Password cannot be same as your current password. Please choose a different password.'], 400);
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();


        return response()->json(['message' => 'Password changed successfully'], 200);
    }

}
